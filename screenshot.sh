#!/bin/bash
# screenshot program for x11
# Ryan D. Knutson 2023
# rynk.xyz


# uncomment the lines below to change the directory SS is stored in
# !!! you MUST use a trailing / !!!
SSPATH="$HOME/screenshots/"


# comment the lines below to change the filename
# save with current date, time
SSNAME="$(date +%m.%d.%y_%H.%M.%S).png"
# save with a unique id
# SSNAME="$(cat /proc/sys/kernel/random/uuid|sed 's/[-]//g'|head -c 20).png"


# where the magic happens
DISPLAY=:0 import -window root "$SSPATH$SSNAME"
notify-send --icon="$SSPATH$SSNAME" "Screenshot saved!" "$SSPATH$SSNAME"
