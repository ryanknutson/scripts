#!/bin/bash
# backup iphone pictures using btrfs snapshots
# Ryan D. Knutson 2023
# rynk.xyz

# FULL path of where to backup
# note that a trailing `/` is required!
iBAKPATH="/home/ryanknut/backup/iphone/"

# FULL path of where btrfs snapshots should be stored
# note that a trailing `/` is required!
iBTRFSSSPATH="/home/ryanknut/backup/snapshots/"

# what snapshots should be titled
# default is iphone-Mon-DD-YYYY
iBAKTITLE="iphone-$(date '+%b-%d-%Y')"



# check if running as root
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
  echo "Not running as root"
  exit 1
fi


# check that iPhone is connected and paired
if idevicepair validate; then
  echo "iPhone paired!"
else
  echo "please connect + pair iPhone with 'idevicepair pair'"
  exit 1
fi


# check that iPhone is mounted
if mount | grep /mnt/iphone; then
  echo "iPhone mounted, starting backup..."
else
  echo "please mount iPhone with 'ifuse /mnt/iphone/'"; exit 1
fi


# start rsync backup
echo "backing up..."
rsync -av --progress "/mnt/iphone/DCIM/" "$iBAKPATH"
echo "backup done!"


# create btrfs snapshot
## check if directory exists. if not, create it
while [ ! -d "iBTRFSSSPATH" ] 
do
  echo "doesn't exist!"
  if ! mkdir -v "iBTRFSSSPATH" ; then
    echo "snapshot directory could not be created!"
    exit 126
  fi
done
## create snapshot
echo "creating snapshot at:"
echo "$iBTRFSSSPATH$iBAKTITLE"
btrfs subvolume snapshot "$iBAKPATH" "$iBTRFSSSPATH$iBAKTITLE"
echo "done!"


# ask user if they want to unmount
read -n1 -p "Unmount iPhone? [y,n] " unmount
case $unmount in
  y|Y) printf "\nunmounting /mnt/iphone\n"; umount /mnt/iphone;;
  n|N) printf "\nnot unmounting\n";;
  *) printf "\ninvalid response, not unmounting\n";;
esac
exit 0
