#!/bin/bash
# volume control with osd support
# Copyright (C) 2023 Ryan D. Knutson
# rynk.xyz/unixuniverse.net

helptext="Usage: $0 up|down|mute|unmute|toggle|set|osd [%]"

# if a percent variable is present, check that it's a valid int
if [ -n "$2" ] && ! [[ $2 =~ ^[0-9]+$ ]]; then
  echo "$2 is not a valid integer"
  echo $helptext
  exit 1
fi

# if a percent variable is present, change the volume by that percent
# if not, use 5% as a default
vol_percent_change=${2:-5}

# commands
case $1 in
  "up")
    echo "Increasing volume by $vol_percent_change%"
    amixer set Master "$vol_percent_change%+" unmute
    ;;
  "down")
    echo "Decreasing volume by $vol_percent_change%-"
    amixer set Master "$vol_percent_change%-" unmute
    ;;
  "mute")
    echo "Muting"
    amixer set Master mute
    ;;
  "unmute")
    echo "Unmuting"
    amixer set Master unmute
    ;;
  "toggle")
    echo "Toggling mute"
    amixer set Master toggle
    ;;
  "set")
    [ -z "$2" ] && echo $helptext && exit
    echo "Setting volume to $2%"
    amixer set Master "$2%"
    ;;
  # just show the osd
  "osd") ;;
  # --help and -h
  "--help" | "-h") echo $helptext && exit ;;
  # invalid argument
  *) [ ! -z "$1" ] && echo $helptext && exit ;;
esac


# osd section

# get the volume % and state of mute (on or off)
state_and_level=$(amixer sget Master | grep "Front Left:" | awk -F'[][]' '{print $4, $2}')


# is output on or off?
state=$(echo "$state_and_level" | awk '{print $1}')

# volume level
level=$(echo "$state_and_level" | awk '{print $2}' | sed 's/%//')
# Set level to 0 if muted
[ "$state" == "off" ] && level=0

# default title
title="Volume"
# Set title to "Muted" if muted
[ "$state" == "off" ] && title="Muted"


# if osd_cat is already running, kill the old instance on current user
current_user=$(whoami)
if pgrep -u "$current_user" -x "osd_cat" > /dev/null; then
  pkill -u "$current_user" -TERM -x "osd_cat"
fi

# show the OSD
# note that you can change the number in the -f option for font size
# you can also set the color. see the list of X11 colors for more
# set -o for positioning
osd_cat --align center --pos bottom --lines 1 -o 175 -T "$title" -b percentage -d 2 -P "$level" -f "-*-*-*-*-*-*-16-*-*-*-*-*-*-*" --color="Magenta" &

# print out info to terminal as well
[ "$state" == "off" ] && echo "Volume: Muted" || echo "Volume: $level"
